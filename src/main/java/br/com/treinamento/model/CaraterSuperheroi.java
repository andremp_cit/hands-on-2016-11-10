package br.com.treinamento.model;

public enum CaraterSuperheroi {
	HEROI(1, "Heroi"),
	VILAO(2, "Vilão");
	
	Integer id;
	String nome;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	CaraterSuperheroi(Integer id, String nome) {
		this.id = id;
		this.nome = nome;
	}
}
