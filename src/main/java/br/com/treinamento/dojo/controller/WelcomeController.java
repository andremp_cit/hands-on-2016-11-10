package br.com.treinamento.dojo.controller;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.treinamento.dojo.dto.EventDataWrapperDto;
import br.com.treinamento.model.CaraterSuperheroi;

@RestController
public class WelcomeController {
	
	private Map<String, CaraterSuperheroi> caraterSuperheroi = new HashMap<String, CaraterSuperheroi>();

	@RequestMapping(value = "/helloworld", method = RequestMethod.GET)
	public ResponseEntity<String> helloWorld() {

		return new ResponseEntity<String>("hello world", HttpStatus.OK);
	}

	
	@RequestMapping(value = "/caraterPersonagem/{idPersonagem}", method = RequestMethod.POST)
	public ResponseEntity<String> informarCaraterPersonagem(
			@PathVariable String idPersonagem,
			@RequestParam("tipo") String tipo) {
		
		Boolean alteradoComSucesso = true;
		
		switch (tipo.toUpperCase()) {
			case "HEROI":
				caraterSuperheroi.put(idPersonagem, CaraterSuperheroi.HEROI);
				break;
			case "VILAO":
				caraterSuperheroi.put(idPersonagem, CaraterSuperheroi.VILAO);
				break;
			default:
				alteradoComSucesso = false;
		}
		
		if (alteradoComSucesso) {
			return new ResponseEntity<String>(HttpStatus.OK);
		} else {
			return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value = "/personagens", method = RequestMethod.GET)
	public ResponseEntity<EventDataWrapperDto> personagens() {
		
		EventDataWrapperDto eventDataWrapperDto = null;
		
		try {
			Client client = ClientBuilder.newClient();
			eventDataWrapperDto = client.target(montaUrlRest())
			        .request(MediaType.APPLICATION_JSON)
			        .get(EventDataWrapperDto.class);
			
			System.out.println(eventDataWrapperDto.getCode());
        } catch (ClientErrorException e) {
            /*System.out.println(e.getStatusCode());
            System.out.println(e.getResponseBodyAsString());*/
        }

		
		return new ResponseEntity<EventDataWrapperDto>(eventDataWrapperDto, HttpStatus.OK);
		
		 /*HttpURLConnection conn = null;
		 String retornoRest = "";
		 
		  try {
		   URL url = new URL(this.montaUrlRest());

		   conn = (HttpURLConnection) url.openConnection();
		   conn.setRequestMethod("GET");
		   conn.setRequestProperty("Accept", "application/json");

		   if (conn.getResponseCode() != 200) {
		    throw new RuntimeException("Failed: HTTP error code: " + conn.getResponseCode());
		   }

		   BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

		   String output = null;
		   
		   while ((output = br.readLine()) != null) {
		    retornoRest += output;
		   }
		   System.out.println("\nRest Return: " + retornoRest);

		  } catch (MalformedURLException e) {
		   e.printStackTrace();
		  } catch (IOException e) {
		   e.printStackTrace();
		  } finally {
		   conn.disconnect();
		  }
		  
		  return new ResponseEntity<String>(retornoRest, HttpStatus.OK);*/
	}
	
	
	private String montaUrlRest(){
		Long timeStamp = new Date().getTime();
		String privateKey = "0259fe854e7eaddc2f66700a90125ea8660631b9";
		String publicKey = "f1d46eb9f76a8f09332363b741dd4d37";
		String hash = generateHash(timeStamp + privateKey + publicKey);
		
		String urlCompleta = "https://gateway.marvel.com:443/v1/public/events/238/characters?ts=" + timeStamp + "&hash="+hash + "&apikey=" + publicKey;
		
System.out.println(">>> " + urlCompleta);
		return urlCompleta;
	}
	
	/**
	 * Gera o hash da String
	 * @param str
	 * @return
	 */
	private static String generateHash(String str) {
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		md.update(str.getBytes());

		byte[] bytes = md.digest();

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < bytes.length; i++) {
			int parteAlta = ((bytes[i] >> 4) & 0xf) << 4;
			int parteBaixa = bytes[i] & 0xf;
			if (parteAlta == 0) {
				sb.append('0');
			}
			sb.append(Integer.toHexString(parteAlta | parteBaixa));
		}
		return sb.toString();

	}
}
